import Repositories.ActorRepository;
import Repositories.GenreRepository;
import hibernate.Actor;
import hibernate.Genre;
import hibernate.Movie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class HibernateApp {
    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Actor.class)
                .addAnnotatedClass(Movie.class)
                .addAnnotatedClass(Genre.class)
                .buildSessionFactory();

        final GenreRepository genreRepository = new GenreRepository(sessionFactory.createEntityManager());
        genreRepository.create(new Genre("Sci-fi",null));
        genreRepository.create(new Genre("Action",null));
        genreRepository.create(new Genre("Drama",null));
        System.out.println(genreRepository.findAll());

        final Session session = sessionFactory.openSession();
        final ActorRepository actorRepository = new ActorRepository(session);
        Actor savedActor = actorRepository.create(new Actor("Andrzej", "Nowak", 1970, null));
        System.out.println(savedActor);

        /*Transaction transaction = null;
        try (Session session = sessionFactory.openSession()){
            transaction = session.beginTransaction();
            // kod wykonujący modyfikacje na bazie
            transaction.commit();
        } catch (Exception e){
            e.printStackTrace();
            if (transaction !=null) {
                transaction.rollback();
            }
        }
        sessionFactory.close();
*/
    }
}
