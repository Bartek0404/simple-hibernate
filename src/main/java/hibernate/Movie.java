package hibernate;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.List;

@Entity (name = "movies")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "actors", callSuper = true)
@ToString(exclude = "actors")

public class Movie extends BaseEntity {

    @Column (name = "movie_title")
    private String title;

    @Column (name = "year_of_release")
    private Integer yearOfRelease;

    @ManyToMany (mappedBy = "movies")
    List<Actor> actors = new ArrayList<>();

    @ManyToOne
    private Genre genre;

}
