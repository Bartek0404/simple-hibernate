package Repositories;

import hibernate.Actor;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class ActorRepository {
    private final EntityManager entityManager;

    public ActorRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Optional<Actor> findById(final UUID id) {
        return  Optional.ofNullable(entityManager.find(Actor.class, id));
    }

    public List<Actor> findByYearOfBirthHigherThan(Integer yearOfBirth) {
        return entityManager.createQuery("SELECT a FROM actors a WHERE a.year_of_birth > :yearOfBirth", Actor.class).setParameter("yearOfBirth", yearOfBirth).getResultList();
    }

    public List<Actor> findByLastNameEndsWith(String lastNameSuffix) {
        return entityManager.createQuery("SELECTS a FROM actors a WHERE a.last_name LIKE :lastNameSuffix", Actor.class).setParameter("lastNameSuffix","%" + lastNameSuffix).getResultList();
    }

    public Actor create(Actor actor) {
        EntityTransaction entityTransaction = null;
        try {
            entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.persist(actor);
            entityTransaction.commit();
            return actor;
        } catch (final Exception e) {
            if (entityTransaction != null) {
                entityTransaction.rollback();
            }
            return null;
        }
    }
}
