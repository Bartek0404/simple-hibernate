package Repositories;

import hibernate.Movie;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class MovieRepository {
    private final EntityManager entityManager;

    public MovieRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Optional<Movie> findById(final UUID id) {
        return Optional.ofNullable(entityManager.find(Movie.class, id));
    }

    public List<Movie> findAll() {
        return entityManager.createQuery("from movies", Movie.class).getResultList();
    }

    public Optional<Movie> findByTitle(String title) {
        return Optional.ofNullable(entityManager.createQuery("SELECT m FROM movies m WHERE m.movie_title = :title", Movie.class).setParameter("title", title).getSingleResult());
    }

    public Movie create(Movie movie) {
        EntityTransaction entityTransaction = null;
        try {
            entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.persist(movie);
            entityTransaction.commit();
            return movie;
        } catch (final Exception e) {
            if (entityTransaction != null) {
                entityTransaction.rollback();
            }
            return null;
        }
    }

    public void delete(Movie movie) {
        EntityTransaction entityTransaction = null;
        try {
            entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.remove(movie);
            entityTransaction.commit();
        } catch (final Exception e) {
            if (entityTransaction != null) {
                entityTransaction.rollback();
            }
        }
    }
}
