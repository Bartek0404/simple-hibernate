package Repositories;

import hibernate.Genre;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class GenreRepository {
    private final EntityManager entityManager;

    public GenreRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Optional<Genre> findById(final UUID id) {
        return Optional.ofNullable(entityManager.find(Genre.class, id));
    }

    public List<Genre> findAll() {
        return entityManager.createQuery("from genres", Genre.class).getResultList();
    }

    public Optional<Genre> findByName(String name) {
        return Optional.ofNullable(entityManager.createQuery("SELECT g FROM genre WHERE name = :name", Genre.class).setParameter("name", name).getSingleResult());
    }

    public Genre create(Genre genre) {
        EntityTransaction entityTransaction = null;
        try {
            entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.persist(genre);
            entityTransaction.commit();
            return genre;
        } catch (final Exception e) {
            if (entityTransaction != null) {
                entityTransaction.rollback();
            }
            return null;
        }
    }

    public void delete(Genre genre) {
        EntityTransaction entityTransaction = null;
        try {
            entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.remove(genre);
            entityTransaction.commit();
        } catch (final Exception e) {
            if (entityTransaction != null) {
                entityTransaction.rollback();
            }
        }
    }
}
